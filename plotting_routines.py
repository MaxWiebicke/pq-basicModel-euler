import numpy, sys
import matplotlib.pyplot as plt

def plotting_testData(data,data2,data3,data4,testType):
    
    if testType=="iso":
        fig, ax = plt.subplots()
        
        ax.plot(data[:,2],data[:,4], '-', label='Simulation')
        ax.set_xlabel('vol stress $p$')
        ax.set_ylabel('solid fraction $\phi$')
        ax.legend()
        
        plt.tight_layout()
        plt.show()
        
    elif testType=="oed":
        fig, ax = plt.subplots()
        
        ax.plot(data[:,2],data[:,4], '-', label='Simulation')
        ax.set_xlabel('vol stress $p$')
        ax.set_ylabel('solid fraction $\phi$')
        ax.legend()
        
        plt.tight_layout()
        plt.show()
        
    elif testType=="txu":
        fig, axs = plt.subplots(2, 2, figsize=(10,10))

        # plot dev elastic stress vs dev strain
        axs[0,0].plot(data[:,1],data[:,3], '-', label='Simulation')
        if data2 is not None:
            axs[0,0].plot(2/3*data2[:,0]/100,data2[:,7], '-', label='Experiment') #wichtmann
        axs[0,0].set_xlabel('dev strain $\epsilon_s$')
        axs[0,0].set_ylabel('dev stress $q$')
        axs[0,0].legend()
                
        # plot stress path
        axs[0,1].plot(data[:,2], data[:,3], '-')
        if data2 is not None:
            axs[0,1].plot(data2[:,6], data2[:,7], '-', label='') #wichtmann
        axs[0,1].set_xlabel('vol stress $p$')
        axs[0,1].set_ylabel('dev stress $q$')        
        
        axs[1,0].plot(data[:,1],data[:,4], '-', label='')
        axs[1,0].set_xlabel('dev strain $\epsilon_s$')
        axs[1,0].set_ylabel('solid fraction $\phi$')
        axs[1,0].invert_yaxis()
        
        axs[1,1].plot(data[:,2],data[:,4], '-', label='')
        axs[1,1].set_xlabel('vol stress $p$')
        axs[1,1].set_ylabel('solid fraction $\phi$')
        axs[1,1].invert_yaxis()
        
        plt.tight_layout()
        plt.show()
        
        
