# code for the pure hydrodynamic model for clay 

import numpy, sys
import matplotlib.pyplot as plt

from hydrodyn_functions import *
from plotting_routines import *

#================================
# Input
#================================

# initial state
strain = numpy.array([0,0])
Tm     = 0    
pe     = 100
qe     = 0
phi    = 0.423

# for Wichtmann
# Karlsruhe Kaolin
K = 14.e6
G = 7.e6
omega = 0.6
M = 0.95
R = 0.543*10**6
Lambda = 10.0

# hydrodyn parameters
#  just for producing insignificant viscous stress and pT
eta   = 1.e3  
alpha = 1.e5
beta  = alpha

modelParam={"K":K, "G":G, "Lambda":Lambda, "R":R, "M":M, "eta":eta, "alpha":alpha, "beta":beta, "omega":omega}

# loading
testType    = "txu"   # list for loading conditions
loadingRate = 1.e-6   # rate of loading
targetInc   = 0.1     # increment of loading

#================================
# Calculation
#================================

# get the initial elastic strain 
strainE = initialElasticStrains(numpy.array([pe,qe]),phi,modelParam)
stateVar = numpy.array([strain[0],strain[1],strainE[0],strainE[1],Tm,phi])
stress = numpy.array([pe,qe]) #assuming we start with Tm=0

t = 0.
# initialise data for plotting
data = numpy.array([[strain[0], strain[1], stress[0], stress[1], phi, t ]])

# integration - simplfied for solely one loading path
# determine step sizes for integration
epsInc = 1.e-4
dt = epsInc/loadingRate
steps = int(targetInc/epsInc)
print("Strain rate          [1/s] = ", loadingRate)
print("Time increment       [-]   = ", dt)
print("Strain increment     [-]   = ", epsInc)
    
for step in range(steps):
    t += dt
    stateVar, strainRate = stateIntegrationEuler(stateVar, modelParam, testType, loadingRate, dt)
    data = recordData(data,stateVar, strainRate,t,modelParam)
    
    step += 1
        
# plotting
# load Wichtmann txu
folder = "./dataExp/wichtmann/"
dataExpM02 = numpy.loadtxt(folder+"M02.dat",skiprows=2)
plotting_testData(data,dataExpM02,None,None,'txu')



