# collection of functions for the constitutive models
import numpy
import math
import sys

# transformation of principal strains into invariants
def eps2pq(eps_1,eps_2):
    eps_p = eps_1+2*eps_2
    eps_q = 2/3*(eps_1-eps_2)
    strainpq = numpy.array([eps_p,eps_q])
    return strainpq

# stress and strain increments on different loading paths
def testControl(loadingRate,testType):        
    if testType=="iso":
        deps_p = 1*loadingRate
        deps_q = 0*loadingRate
        
    elif testType=='oed':
        deps_1 = 1*loadingRate
        deps_2 = 0*loadingRate
        deps_p, deps_q = eps2pq(deps_1,deps_2)
        
    elif testType=='txu':
        deps_1 = 1*loadingRate
        deps_2 = -0.5*loadingRate
        deps_p, deps_q = eps2pq(deps_1,deps_2)
        
    dstrain=numpy.array([deps_p,deps_q])
    
    return dstrain

def elasticStress(phi,strainE,modelParam):
    K = modelParam["K"]
    G = modelParam["G"]
    pe = phi**6/2*(K*strainE[0]**2 + 3*G*strainE[1]**2)
    qe = phi**6*3*G*strainE[0]*strainE[1]
    
    return numpy.array([pe,qe])
    
def viscousStress(Tm,strainRate,modelParam):
    alpha = modelParam["alpha"]
    beta  = modelParam["beta"]
    
    pd = alpha*Tm*strainRate[0]
    qd = beta*Tm*strainRate[1]
    
    return numpy.array([pd,qd])

def initialElasticStrains(stress,phi,modelParam):
    pe = stress[0]
    qe = stress[1]
        
    K = modelParam["K"]
    G = modelParam["G"]
    
    x = 1/phi**6 * ( pe*2/(2*K) + math.sqrt( (pe/K)**2 - qe**2/(3*G*K) ) )
    epse_v = x**(1/2)
    epse_s = qe/(3*G*phi**6*epse_v)
        
    return numpy.array([epse_v,epse_s])
    
def plastCoefficients(stress,Tm,phi,modelParam):
    eta    = modelParam["eta"]
    Lambda = modelParam["Lambda"]
    R      = modelParam["R"]
    M      = modelParam["M"]
    omega  = modelParam["omega"]
    
    alpha, beta = modelParam["alpha"], modelParam["beta"]
    
    pc = R*phi**Lambda
    
    a = math.sqrt(eta/alpha)/pc * (stress[0]/pc)**(0)
    b = -a/M*((stress[1])/(M*stress[0]))**(1)
    c = math.sqrt(eta/beta)/(M*omega*pc) + a/M**2
    
    # check Onsager 
    if a*c-b**2 < 0:
        print("Onsager's requirement not satisfied.")
        print("a*c-b**2 > 0 ....")
        print(stress)
        sys.exit()
    
    return a,b,c
    
def plasticStrainRate(stress,dStrain,Tm,phi,modelParam):
    a,b,c = plastCoefficients(stress,Tm,phi,modelParam)
    A = numpy.array([[a,b],[b,c]])
    dStrainP = Tm*numpy.dot(A,stress)
    
    return dStrainP
    
def TmRate(Tm,dStrain,modelParam):
    alpha, beta, eta = modelParam["alpha"], modelParam["beta"], modelParam["eta"]
    dTm = (alpha*dStrain[0]**2 + beta*dStrain[1]**2 - eta*Tm**2)/2
    
    return dTm

def stateIntegrationEuler(stateVar,modelParam, testType, loadingRate, dt):
    strain  = stateVar[0:2]
    strainE = stateVar[2:4]
    Tm      = stateVar[4]
    phi     = stateVar[5]
    
    dStrain = testControl(loadingRate,testType)
    stress  = elasticStress(phi,strainE,modelParam)
    
    # rate of states
    dStrainE = dStrain - plasticStrainRate(stress,dStrain,Tm,phi,modelParam)
    dPhi     = phi * dStrain[0] 
    dTm      = TmRate(Tm,dStrain,modelParam)
    
    # update state variables
    strain  = strain + dStrain * dt
    strainE = strainE + dStrainE * dt
    Tm      = Tm + dTm * dt
    phi     = phi + dPhi * dt
    
    return numpy.hstack((strain[0],strain[1],strainE[0],strainE[1],Tm,phi)), dStrain
    
        
def recordData(data,stateVar,strainRate,t,modelParam):

    stressE = elasticStress(stateVar[5],stateVar[2:4],modelParam)
    stressD = viscousStress(stateVar[4],strainRate,modelParam)
    stress  = stressE + stressD + numpy.array([stateVar[4]**2,0])
    data = numpy.append(data,[[stateVar[0], stateVar[1], stress[0], stress[1], stateVar[5], t ]], axis=0)

    return data

